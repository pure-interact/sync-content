<?php

namespace SyncContent\Controllers;

use Exception;
use SyncContent\Services\SyncContentService;

class SyncContentController
{
    /**
     * @var SyncContentService
     */
    private $syncContentService;

    public function __construct(SyncContentService $syncContentService)
    {
        $this->syncContentService = $syncContentService;
    }

    /**
     * Todo: Add secret check to prevent calls...
     * Todo: ```SELECT `ID`, `post_excerpt`, `post_content` FROM `wp_posts` WHERE `post_excerpt` LIKE '%wp-content%' OR `post_content` LIKE '%wp-content%'``` sync wp-content
     * @throws Exception
     */
    public function index(): void
    {
        global $wpdb;

        // Start Transaction
        $wpdb->query("START TRANSACTION");

        try {
            $data = $this->syncContentService->process();

            // All ok, save the changes
            $wpdb->query( "COMMIT" );
//            $wpdb->query( "ROLLBACK" );
        } catch (Exception $exception) {
            $wpdb->query( "ROLLBACK" );

            throw $exception;
        }

        header("Content-Type: application/json; charset=UTF-8");
        echo json_encode([
            'message' => 'Sync completed',
            'data' => array_map(static function (object $item) {
                return ['id' => (int)$item->ID];
            }, $data),
        ]);
        die();
    }
}
