<?php

namespace SyncContent\Services;

use PiWpBasePlugin\Repositories\PostRepository;
use RuntimeException;
use WP_Error;

/**
 * Class SyncContentService
 * @package SyncContent\Services
 */
class SyncContentService
{
    private $forcedSync = false;

    private const MAPPED_POST_TYPE_KEYS = [
        'ctc_sermon' => 'sermon',
        'attachment' => 'attachment'
    ];

    private const MAPPED_POST_META_KEYS = [
        '_ctc_sermon_pdf' => 'pdf_url',
        '_ctc_sermon_video' => 'video_url',
        '_ctc_sermon_audio' => 'audio_url',
        '_ctc_sermon_audio_id' => 'audio_attachment_id',
        'duration' => 'audio_duration',
        'filesize' => 'audio_filesize',
        'filesize_raw' => 'audio_filesize_raw',
        'date_recorded' => 'recorded_at',
        'cover_image' => 'social_thumbnail',
        '_thumbnail_id' => '_thumbnail_id',

        // YOAST SEO FIELDS
        '_yoast_wpseo_title' => '_yoast_wpseo_title',
        '_yoast_wpseo_content_score' => '_yoast_wpseo_content_score',
        '_yoast_wpseo_focuskw' => '_yoast_wpseo_focuskw',
        '_yoast_wpseo_focuskw_text_input' => '_yoast_wpseo_focuskw_text_input',
        '_yoast_wpseo_linkdex' => '_yoast_wpseo_linkdex',
        '_yoast_wpseo_meta-robots-nofollow' => '_yoast_wpseo_meta-robots-nofollow',
        '_yoast_wpseo_meta-robots-noindex' => '_yoast_wpseo_meta-robots-noindex',
        '_yoast_wpseo_metadesc' => '_yoast_wpseo_metadesc',
    ];

    private const MAPPED_TAXONOMY_KEYS = [
        'ctc_sermon_speaker' => 'cc_sermon_speaker',
        'ctc_sermon_series' => 'cc_sermon_series',
        'series' => 'cc_sermon_series',
        'ctc_sermon_book' => 'cc_sermon_book',
        'ctc_sermon_tag' => 'cc_sermon_tag'
    ];

    /** @var PostRepository */
    private $postRepository;

    /**
     * SyncContentService constructor.
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @return array
     */
    public function process(): array
    {
        $this->forcedSync = true;

        $posts = $this->postRepository->findAllWithRelationships([
            'postType' => 'ctc_sermon', // Todo: define in plugin-config
            'id' => '445171' // Todo: Remove
        ], true);

        $fetchedPosts = [];
        foreach ($posts as $srcPost) {
            $distPost = $this->processSourcePost($srcPost);

            if ($distPost !== null) {
                $fetchedPosts[] = $distPost;
            }

            if (count($fetchedPosts) >= 5) {
                break; // Break process after 5 syncs
            }
        }

        return $fetchedPosts;
    }

    /**
     * @param $srcPost
     * @return object|null
     */
    public function processSourcePost($srcPost): ?object
    {
        $existingPost = $this->postRepository->findExistingPostBySrcPostId($srcPost->ID);

        // If post is not update to date..
        $syncPostModifiedDate = $existingPost->metadata['sync_src_post_modified'] ?? null;

        // Ignore actual posts
        if (($srcPost->post_modified === $syncPostModifiedDate) && $this->forcedSync === false) {
            return null;
        }

        $distPost = $this->cudPost($srcPost, $existingPost);
        if ($distPost === null) {
            throw new RuntimeException(
                sprintf('Failed to sync source post "%s" not found', $srcPost->ID)
            );
        }

        $this->cudPostMeta($srcPost, $distPost);
        $this->cudPostAttachments($srcPost, $distPost);

        if ($distPost->post_type !== 'attachment') {
            $this->updatePostMetaAttachmentRelations($distPost);
        }

        $this->updatePostTaxonomyTerms($srcPost, $distPost);

        return $distPost;
    }

    /**
     * @param object $srcPost
     * @param object|null $existingPost
     * @param object|null $distParentPost
     * @return Object|null
     */
    public function cudPost(object $srcPost, ?object $existingPost = null, object $distParentPost = null): ?object
    {
        $distPost = clone $srcPost; // Create new reference
        $srcPostId = $distPost->ID;
        unset($distPost->ID);

        $distPost->post_author = 1;
        $distPost->post_type = self::MAPPED_POST_TYPE_KEYS[$distPost->post_type] ?? $distPost->post_type;

        if ($distPost->post_type !== 'attachment') {
            unset($distPost->guid);

            $this->replaceAttachmentsInContent($distPost);
        }

        if ($existingPost === null) {
            $succeeded = wp_insert_post((array)$distPost);
            if ($succeeded instanceof WP_Error) {
                throw new RuntimeException(
                    $succeeded->get_error_message(),
                    $succeeded->get_error_code()
                );
            }

            $distPost->metadata = [];
            $distPost->attachments = [];
            $distPost->taxonomies = [];

            if ($succeeded) {
                // Register source PostId
                update_post_meta($succeeded, 'sync_src_post_id', $srcPostId);
                $distPost->metadata['sync_src_post_id'] = $srcPostId;

                update_post_meta($succeeded, 'sync_src_post_modified', $srcPost->post_modified);
                $distPost->metadata['sync_src_post_modified'] = $srcPost->post_modified;
            }

            $distPost->ID = $succeeded;

            // Add attachments to parentPost
            if ($distParentPost !== null && $distPost->post_type === 'attachment') {
                $distParentPost->attachments[$distPost->ID] = $distPost;
            }

            return $distPost;
        }

        // Update existing Post with source values
        $distPost->ID = $existingPost->ID;
        $distPost->attachments = $existingPost->attachments;
        $distPost->taxonomies = $existingPost->taxonomies;
        $distPost->metadata = $existingPost->metadata;

        $succeeded = wp_update_post((array)$distPost);
        if ($succeeded instanceof WP_Error) {
            throw new RuntimeException(
                $succeeded->get_error_message(),
                $succeeded->get_error_code()
            );
        }

        update_post_meta($succeeded, 'sync_src_post_modified', $srcPost->post_modified);
        $distPost->metadata['sync_src_post_modified'] = $srcPost->post_modified;

        return $distPost;
    }

    /**
     * @param array $metadata
     * @return array
     */
    private function filteredAndMappedMetadata(array $metadata): array
    {
        $filteredAndMappedMetadata = [];
        foreach ($metadata as $key => $value) {
            if (!array_key_exists($key, self::MAPPED_POST_META_KEYS)) {
                continue;
            }

            $mappedKey = self::MAPPED_POST_META_KEYS[$key] ?? $key;
            $filteredAndMappedMetadata[$mappedKey] = $value;
        }

        return $filteredAndMappedMetadata;
    }

    /**
     * @param object $srcPost
     * @param object $distPost
     * @return void
     */
    private function cudPostMeta(object $srcPost, object $distPost): void
    {
        $metadata = $this->filteredAndMappedMetadata($srcPost->metadata);

        foreach ($metadata as $key => $value) {
            update_post_meta($distPost->ID, $key, $value);
            $distPost->metadata[$key] = $value;
        }

        $metadataToDelete = array_keys(array_diff_key($distPost->metadata, $metadata));
        foreach ($metadataToDelete as $key) {
            if ($key !== 'sync_src_post_id') {
                delete_post_meta($distPost->ID, $key);
            }
        }
    }

    /**
     * Todo: Match files before download that the are not equal
     * @param object $distAttachment
     * @return object
     */
    private function downloadAttachment(object $distAttachment): object
    {
        global $wpdb;

        $info = pathinfo($distAttachment->guid);
        $uploadPath = '/' . strstr($info['dirname'], 'wp-content', false) . '/';
        $filename = $uploadPath . $info['basename'];
        $srcGuid = $distAttachment->guid;

        $distAttachment->guid = get_site_url() . $filename;

        // @todo: research if there is an alternative option..
        // Overwrite guid to set correct reference after file transfer (because WP-core won't do that)
        $prefix = $wpdb->prefix;
        $wpdb->query("
            UPDATE {$prefix}posts
                SET guid = '{$distAttachment->guid}'
            WHERE ID = '" . $distAttachment->ID . "'
        ");



        $this->downloadFileFromSource($srcGuid, '.' . $filename);

        return $distAttachment;
    }

    /**
     * @param $sourceUrl
     * @param $filename
     * @return void
     */
    private function downloadFileFromSource($sourceUrl, $filename): void
    {
        $sourceSha1 = @sha1_file($sourceUrl);
        $distPathSha1 = @sha1_file($filename);

        // Ignore if existing file equals
        if ($sourceSha1 === $distPathSha1) {
            return;
        }

        $distPath = dirname($filename);

        if (
            !file_exists($distPath)
            && !mkdir($distPath, 0777, true)
            && !is_dir($distPath)
        ) {
            throw new RuntimeException(sprintf('Failed to create directory "%s".', $distPath));
        }

        if ($data = @file_get_contents($sourceUrl)) {
            file_put_contents($filename, $data);
            return;
        }

        throw new RuntimeException(sprintf('Failed to download resource "%s"', $sourceUrl));
    }

    /**
     * @param object $distPost
     * @return void
     */
    private function updatePostMetaAttachmentRelations(object $distPost): void
    {
        foreach ($distPost->metadata as $key => $meta) {
            if (strpos($meta, '/wp-content/uploads') === false) {
                continue;
            }

            foreach ($distPost->attachments as $attachment) {
                if ($meta !== $attachment->guid) {
                    continue;
                }

                $distPost->metadata[$key . '_id'] = $attachment->ID;
                update_post_meta($distPost->ID, $key . '_id', $attachment->ID);
            }
        }
    }

    /**
     * @param object $srcPost
     * @param object $distPost
     * @return void
     */
    private function cudPostAttachments(object $srcPost, object $distPost): void
    {
        foreach ($srcPost->attachments as $attachment) {
            $attachment->post_parent = $distPost->ID;

            $srcAttachmentId = $attachment->ID;
            $srcAttachmentGuid = $attachment->guid;

            $existingAttachment = $this->postRepository->findExistingPostBySrcPostId($attachment->ID);

            $distAttachment = $this->cudPost($attachment, $existingAttachment, $distPost);

            // If post is not update to date..
            $syncPostModifiedDate = $existingAttachment->metadata['sync_src_post_modified'] ?? null;

            // Ignore actual posts
            if (($attachment->post_modified === $syncPostModifiedDate) && $this->forcedSync === false) {
                return;
            }

            if ($distAttachment === null) {
                throw new RuntimeException('Attachment not found');
            }

            $distAttachment->srcAttachmentGuid = $srcAttachmentGuid;

            $distAttachment = $this->downloadAttachment($distAttachment);

            // Reference also metadata to the new created attachment.
            foreach ($distPost->metadata as $key => &$metaValue) {
                // Update attachment id with updated attachment id
                if ((int)$metaValue === (int)$srcAttachmentId && substr($key, -3) === '_id') {
                    update_post_meta($distPost->ID, $key, $distAttachment->ID);
                    $metaValue = $distAttachment->ID;
                }

                // Update metadata with update guid;
                if ($metaValue === $srcAttachmentGuid) {
                    update_post_meta($distPost->ID, $key, $distAttachment->guid);
                    $metaValue = $distAttachment->guid;
                }
            }
            unset ($metaValue);
        }

        $srcAttachmentIds = array_values(array_map(static function ($attachment) {
            return $attachment->ID;
        }, $srcPost->attachments));

        // Verwijderen bijlagen die niet meer in de source zitten.
        foreach ($distPost->attachments as $attachment) {
            $srcAttachmentId = $attachment->metadata['sync_src_post_id'] ?? null;
            if ($srcAttachmentId === null) {
                continue;
            }

            if (!in_array($srcAttachmentId, $srcAttachmentIds, true)) {
                $succeeded = wp_delete_attachment($attachment->ID);
            }
        }
    }

    /**
     * @param object $srcPost
     * @param object $distPost
     * @return void
     */
    private function updatePostTaxonomyTerms(object $srcPost, object $distPost): void
    {
        foreach ($srcPost->taxonomies as $srcTaxonomy => $terms) {
            $taxonomy = self::MAPPED_TAXONOMY_KEYS[$srcTaxonomy] ?? null;
            if ($taxonomy === null) {
                throw new RuntimeException('Failed to map taxonomy ' . $srcTaxonomy);
            }

            // Create term if not alreay exists
            foreach ($terms as $term) {
                wp_insert_term($term, $taxonomy);
            }

            $succeeded = wp_set_object_terms($distPost->ID, $terms, $taxonomy);
            if ($succeeded instanceof WP_Error) {
                throw new RuntimeException(
                    $succeeded->get_error_message()
                );
            }
        }
    }

    private function replaceAttachmentsInContent(object $distPost): void
    {
        $content = $distPost->post_content;
        $pattern = '/https?:\/\/.*(\/wp-content\/.*.(jpg|jpeg|png))/m';
        $matches = [];
        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);

        $siteUrl = get_site_url();

        foreach ($matches as $match) {
            $this->downloadFileFromSource($match[0], './' . $match[1]);
            $content = str_replace($match[0], $siteUrl . $match[1], $content);
        }
    }
}
