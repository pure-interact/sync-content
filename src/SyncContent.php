<?php

namespace SyncContent;

use PiWpBasePlugin\Repositories\PostRepository;
use Routes;
use SyncContent\Controllers\SyncContentController;
use SyncContent\Services\SyncContentService;

class SyncContent
{
    public static function init(): void
    {
        $obj = new self();

        add_action('init', static function () use ($obj) {
            $obj->registerRoutes();
        });
    }

    private function registerRoutes(): void
    {
        Routes::map('sync-content', function () {
            (new SyncContentController(new SyncContentService(new PostRepository())))->index();
        });
    }
}
