<?php
/*
 * Plugin Name: Sync content
 * Version:     1.0.0-beta.0
 * Description: Plugin to Sync Wordpress data
 * Author:      Sander Verhoeve <sander@pureinteract.nl>
 * Author URI:  https://pureinteract.nl
 * Text Domain: sync-content
 * Domain Path: /translations/
 */

use SyncContent\SyncContent;

register_activation_hook(__FILE__, static function () {
    // Install composer dependencies
    $cmd = sprintf('composer install -d "%s"', __DIR__);
    system($cmd, $resultCode);
});

require_once __DIR__ . '/vendor/autoload.php';

SyncContent::init();
